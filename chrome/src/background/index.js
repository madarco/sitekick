import ExpiryMap from "expiry-map";
import { v4 as uuidv4 } from "uuid";
import { fetchSSE } from "./fetch-sse.js";
import { Buffer} from 'buffer'
import { Howl } from "howler";

const KEY_ACCESS_TOKEN = "accessToken";

let prompt =
  "You are acting as a summarization AI, and for the input text please summarize it to the most important 3 to 5 bullet points for brevity, and also extract reviews or comments from the page of what other shoppers are saying: ";
let apiKey = "";
chrome.storage.sync.get(["prompt", "apiKey"], function (items) {
  if (items && items.prompt) {
    prompt = items.prompt;
  }
  if (items && items.apiKey) {
    apiKey = items.apiKey;
  }
});

const cache = new ExpiryMap(10 * 1000);

async function getAccessToken() {
  if (cache.get(KEY_ACCESS_TOKEN)) {
    return cache.get(KEY_ACCESS_TOKEN);
  }
  const resp = await fetch("https://chat.openai.com/api/auth/session")
    .then((r) => r.json())
    .catch(() => ({}));
  if (!resp.accessToken) {
    throw new Error("UNAUTHORIZED");
  }
  cache.set(KEY_ACCESS_TOKEN, resp.accessToken);
  return resp.accessToken;
}

async function getConversationId() {
  const accessToken = await getAccessToken();
  const resp = await fetch(
    "https://chat.openai.com/backend-api/conversations?offset=0&limit=1",
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
    }
  )
    .then((r) => r.json())
    .catch(() => ({}));
  if (resp?.items?.length === 1) {
    return resp.items[0].id;
  }
  return "";
}

async function deleteConversation(conversationId) {
  const accessToken = await getAccessToken();
  const resp = await fetch(
    `https://chat.openai.com/backend-api/conversation/${conversationId}`,
    {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      },
      body: JSON.stringify({ is_visible: false }),
    }
  )
    .then((r) => r.json())
    .catch(() => ({}));
  if (resp?.success) {
    return true;
  }
  return false;
}

async function getSummary(question, callback) {
  const accessToken = await getAccessToken();
  const messageJson = {
    action: "next",
    messages: [
      {
        id: uuidv4(),
        author: {
          role: "user",
        },
        role: "user",
        content: {
          content_type: "text",
          parts: [question],
        },
      },
    ],
    model: "text-davinci-002-render",
    parent_message_id: uuidv4(),
  };
  await fetchSSE("https://chat.openai.com/backend-api/conversation", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`,
    },
    body: JSON.stringify(messageJson),
    onMessage(message) {
      if (message === "[DONE]") {
        return;
      }
      try {
        const data = JSON.parse(message);
        console.log("Response from openai")
        const text = data.message?.content?.parts?.[0];
        if (text) {
          callback(text);
        }
      } catch (err) {
        console.log("sse message", message);
        console.log(`Error in onMessage: ${err}`);
      }
    },
    onError(err) {
      console.log(`Error in fetchSSE: ${err}`);
    },
  });
}

let preventInstance = {};
function executeScripts(tab) {
  const tabId = tab.id;
  // return if we've already created the summary for this website
  if (preventInstance[tabId]) return;

  preventInstance[tabId] = true;
  setTimeout(() => delete preventInstance[tabId], 10000);

  // Add a badge to signify the extension is in use
  chrome.action.setBadgeBackgroundColor({ color: [242, 38, 19, 230] });
  chrome.action.setBadgeText({ text: "GPT" });

  chrome.scripting.executeScript({
    target: { tabId },
    files: ["content.bundle.js"],
  });

  setTimeout(function () {
    chrome.action.setBadgeText({ text: "" });
  }, 1000);
}

// Load on clicking the extension icon
chrome.action.onClicked.addListener(executeScripts);

// Auto load on page laod:
chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
  if (changeInfo.status == 'complete' && tab.active) {
    console.log("Tab loaded")
    executeScripts(tab);
  }
});

// Listen for messages
chrome.runtime.onConnect.addListener((port) => {
  port.onMessage.addListener(async (request, sender, sendResponse) => {
    await doAction(request.content, port)
  });
});

// Do the actual thing:
async function doAction(content, port) {
  console.debug("received msg ", content);
    try {
      const maxLength = 3000;
      const chunks = splitTextIntoChunks(content, maxLength).slice(0, 2);
      
      let currentSummary = "";
      for (const chunk of chunks) {
        const gptQuestion = prompt + `\n\n${chunk}`;
        let currentAnswer = "";
        await getSummary(gptQuestion, (answer) => {
          currentAnswer = answer;
          combineSummaries([currentSummary, answer])
        });
        await deleteConversation(await getConversationId());
        currentSummary =
          combineSummaries([currentSummary, currentAnswer]) + "\n\n";

        console.log("currentSummary final", currentSummary);

        //THIS actually add the text inside the box:
        /*port.postMessage({
            answer: currentSummary
        });*/
      }

      console.log("Finished summarizing", currentSummary)
      const prompt2 = "You are a snarky Scottish friend, give me a short sentence with a snarky comment about the product I'm going to buy. It should be funny, annoying and something personal about me, for eg. mentioning that I can't afford the product, or that I'm too lazy for this, or I don't wear good enough to use this."
      "\nThis is the product description:\n"
      const gptQuestion = prompt2 + `\n\n${currentSummary}`;
      let answer2 = ""
      await getSummary(gptQuestion, (answer) => {
        answer2 = answer
      });

      //a POST request to https://api.elevenlabs.io/v1/text-to-speech/6pXcaMWiG7EtjPxTEn7M
      /* with the body: {
      "text": "hello!",
      "model_id": "eleven_monolingual_v1",
      "voice_settings": {
        "stability": 0,
        "similarity_boost": 0
      }
    } */
      const response = await fetch('https://api.elevenlabs.io/v1/text-to-speech/6pXcaMWiG7EtjPxTEn7M', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'xi-api-key': '0838bcce214f1e21ce8b091378be19c0',
          'accept': 'audio/mpeg'
        },
        body: JSON.stringify(
          {
            "text": answer2,
            "model_id": "eleven_monolingual_v1",
            "voice_settings": {
              "stability": 0,
              "similarity_boost": 0
            }
          }
        ),
      });
      const blob = await response.blob()
      const base64Str = await blobToBase64(blob)
      console.log("Sound created", base64Str)

      port.postMessage({
        answer: answer2,
        sound: base64Str
      })

    } catch (err) {
      console.error(err);
      port.postMessage({ error: err.message });
      cache.delete(KEY_ACCESS_TOKEN);
    }
}

  //port.onMessage.addListener(async (request, sender, sendResponse) => {



function blobToBase64(blob) {
  return new Promise((resolve, _) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result);
    reader.readAsDataURL(blob);
  });
}

function splitTextIntoChunks(text, maxLength) {
  const chunks = [];
  const words = text.split(/\s+/);
  let currentChunk = "";

  for (const word of words) {
    if (currentChunk.length + word.length + 1 <= maxLength) {
      currentChunk += (currentChunk ? " " : "") + word;
    } else {
      chunks.push(currentChunk);
      currentChunk = word;
    }
  }

  if (currentChunk) {
    chunks.push(currentChunk);
  }

  return chunks;
}

function combineSummaries(summaries) {
  let combinedSummary = "";
  for (const summary of summaries) {
    combinedSummary += (combinedSummary ? " " : "") + summary;
  }

  return combinedSummary;
}
